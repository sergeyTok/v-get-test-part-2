import request from './index';

export const Movie = {
  getAllMovies() {
    return request({
      method: 'get',
      url: `movies`
    })
  },
  getMovieById(movieId) {
    return request({
      method: 'get',
      url: `movies`,
      params: {
        movie_id: movieId
      }
    })
  },
  getMoviesByParams(params) {
    const checkParams = Object
      .keys(params)
      .filter((key) => !!params[key])

    if (checkParams.length === 0) {
      return this.getAllMovies();
    }

    return request({
      method: 'get',
      url: `movies/find`,
      params
    });
  }
}
