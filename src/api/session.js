import request from './index';

export const Session = {
  getSessionsByMovieId(movieId) {
    return request({
      method: 'get',
      url: `movieShows`,
      params: {
        movie_id: movieId
      }
    });
  },
  getSessions() {
    return request({
      method: 'get',
      url: `movieShows`
    })
  },
  postPicket(data) {
    return request({
      method: 'post',
      url: `bookingPlace`,
      data
    });
  }
}
