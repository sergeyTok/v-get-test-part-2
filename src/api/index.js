import axios from 'axios';

const base = axios.create({
  baseURL: process.env.VUE_APP_API_PATH
});

export default base.request;
