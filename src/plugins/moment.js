import Vue from 'vue';
import moment from 'moment';

const MomentPlugin = {};

MomentPlugin.install = function(Vue, options) {
  window.moment = moment
  Vue.moment = moment;

  Object.defineProperties(Vue.prototype, {
    moment: {
      get () {
        return moment
      }
    },
    $moment: {
      get () {
        return moment
      }
    }
  });
}

Vue.use(MomentPlugin);

export default MomentPlugin;
