import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/movies/list'
    },
    {
      path: '/movies/list',
      name: 'Movies list',
      component: () => import('./views/Movies/List.vue')
    },
    {
      path: '/movies/:movieId',
      name: 'Single movie',
      component: () => import('./views/Movies/Single.vue')
    }
  ]
})
