import Vue from 'vue'
import Vuex from 'vuex'
import { MOVIE_STORE_MODULE } from './modules/movie/movie'
import { SESSION_STORE_MODULE } from './modules/sessions/session'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    movieModule: MOVIE_STORE_MODULE,
    sessionModule: SESSION_STORE_MODULE
  },
  state: {},
  mutations: {},
  actions: {}
})
