import { Movie } from '../../../api/movie'
import * as Types from './constants'
import { toData } from '../utils'

const state = {
  movies: [],
  singleMovie: null,
  isPending: false
}

const getters = {
  movies: (state) => state.movies,
  oneMovie: (state) => state.singleMovie
}

const mutations = {
  [Types.GET_ALL_MOVIES]: (state, data = []) => {
    state.isPending = false;
    state.movies = data;
  },
  [Types.SET_SINGLE_MOVIE]: (state, movie) => {
    state.singleMovie = movie;
  },
  SET_PENDING_MOVIES: (state, status) => {
    state.isPending = status;
  }
}

const actions = {
  getAllMovies: async ({ commit }) => {
    commit('SET_PENDING_MOVIES', true);

    return await Movie
      .getAllMovies()
      .then(toData)
      .then((data) => {
        commit(Types.GET_ALL_MOVIES, data);

        return Promise.resolve(data);
      })
  },
  getMovieById: async ({ commit }, movieId) => {
    return await Movie.getMovieById(movieId)
      .then(toData)
      .then((data) => {
        commit(Types.SET_SINGLE_MOVIE, data);

        return Promise.resolve(data);
      })
  },
  getMoviesByParams: async ({ commit }, params) => {
    commit('SET_PENDING_MOVIES', true);

    return await Movie.getMoviesByParams(params)
      .then(toData)
      .then((data) => {
        commit(Types.GET_ALL_MOVIES, data);

        return Promise.resolve(data.length > 0 ? data : 'None results');
      })
  }
}

export const MOVIE_STORE_MODULE = {
  state,
  mutations,
  getters,
  actions
}
