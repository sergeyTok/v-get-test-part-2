import * as Types from './constants'
import { Session } from '../../../api/session'
import { toData } from '../utils'

const state = {
  session: {},
  sessions: []
}

const getters = {
  session: (state) => state.session,
  sessions: (state) => state.sessions
}

const mutations = {
  [Types.GET_MOVIE_SESSION]: (state, data = {}) => {
    state.session = data;
  },
  [Types.GET_ALL_SESSIONS]: (state, data) => {
    state.sessions = data;
  }
}

const actions = {
  getSessionsByMovieId: async ({ commit }, movieId) => {
    return await Session.getSessionsByMovieId(movieId)
      .then(toData)
      .then((data) => {
        commit(Types.GET_MOVIE_SESSION, data);

        return Promise.resolve(data);
      })
  },
  getAllSessions: async ({ commit }) => {
    return await Session.getSessions()
      .then(toData)
      .then((data) => {
        commit(Types.GET_ALL_SESSIONS, data);

        return Promise.resolve(data);
      })
  },
  buyTicket: async ({ commit }, ticketInfo) => {
    return await Session.postPicket(ticketInfo)
      .then((response) => response.data)
      .then((data) => {
        return Promise.resolve(data);
      })
  }
}

export const SESSION_STORE_MODULE = {
  state,
  mutations,
  getters,
  actions
}
